#ifndef NQS_HYPERCUBE_HH
#define NQS_HYPERCUBE_HH

#include <vector>
#include <iostream>
#include <cassert>
#include <map>

namespace nqs{

class Hypercube{

  //edge of the hypercube
  const int L_;

  //number of dimensions
  const int ndim_;

  //whether to use periodic boundary conditions
  const bool pbc_;

  //contains sites coordinates
  std::vector<std::vector<int>> sites_;

  //maps coordinates to site number
  std::map<std::vector<int>,int> coord2sites_;


  //adjacency list
  std::vector<std::vector<int>> adjlist_;

  int nsites_;

public:

  Hypercube(int L,int ndim,bool pbc=true):L_(L),ndim_(ndim),pbc_(pbc){
    assert(L>0);
    assert(ndim>=1);

    GenerateLatticePoints();
    GenerateAdjacencyList();
  }

  void GenerateLatticePoints(){

    std::vector<int> coord(ndim_,0);

    nsites_=0;
    do{
      sites_.push_back(coord);
      coord2sites_[coord]=nsites_;
      nsites_++;
    }
    while( next_variation(coord.begin(), coord.end(), L_-1) );

  }

  void GenerateAdjacencyList(){
    adjlist_.resize(nsites_);

    for(int i=0;i<nsites_;i++){
      std::vector<int> neigh(ndim_);

      neigh=sites_[i];

      for(int d=0;d<ndim_;d++){
        if(pbc_){
          neigh[d]=(sites_[i][d]+1)%L_;
          int neigh_site=coord2sites_.at(neigh);
          adjlist_[i].push_back(neigh_site);
          adjlist_[neigh_site].push_back(i);
        }
        else{
          if((sites_[i][d]+1)<L_){
            neigh[d]=(sites_[i][d]+1);
            int neigh_site=coord2sites_.at(neigh);
            adjlist_[i].push_back(neigh_site);
            adjlist_[neigh_site].push_back(i);
          }
        }

        neigh[d]=sites_[i][d];
      }

    }
  }

  //Returns a list of permuted sites equivalent with respect to
  //translation symmetry
  std::vector<std::vector<int>> SymmetryTable()const{

    if(!pbc_){
      std::cerr<<"Cannot generate translation symmetries in the hypercube withoub PBC"<<std::endl;
      std::abort();
    }

    std::vector<std::vector<int>> permtable;

    std::vector<int> transl_sites(nsites_);
    std::vector<int> ts(ndim_);

    for(int i=0;i<nsites_;i++){
      for(int p=0;p<nsites_;p++){
        for(int d=0;d<ndim_;d++){
          ts[d]=(sites_[i][d]+sites_[p][d])%L_;
        }
        transl_sites[p]=coord2sites_.at(ts);
      }
      permtable.push_back(transl_sites);
    }
    return permtable;
  }

  int Nsites()const{
    return nsites_;
  }

  std::vector<std::vector<int>> Sites()const{
    return sites_;
  }

  std::vector<std::vector<int>> AdjacencyList()const{
    return adjlist_;
  }

  int Coord2Site(const std::vector<int> & coord)const{
    return coord2sites_.at(coord);
  }

  bool IsBipartite()const{
    return true;
  }

  //returns the distances of each point from the others
  std::vector<std::vector<int>> Distances()const{
    std::vector<std::vector<int>> distances;

    for(int i=0;i<nsites_;i++){
      distances.push_back(FindDist(adjlist_,i));
    }

    return distances;
  }

  //From StackOverflow, Matteo Gattanini's code
  // Variations with repetition in lexicographic order
  // k: length of alphabet (available symbols)
  // n: number of places
  // The number of possible variations (cardinality) is k^n (it's like counting)
  // Sequence elements must be comparable and increaseable (operator<, operator++)
  // The elements are associated to values 0÷(k-1), max=k-1
  // The iterators are at least bidirectional and point to the type of 'max'
  template <class Iter>
  bool next_variation(Iter first, Iter last, const typename std::iterator_traits<Iter>::value_type max)
  {
    if(first == last) return false; // empty sequence (n==0)

    Iter i(last); --i; // Point to the rightmost element
    // Check if I can just increase it
    if(*i < max) { ++(*i); return true; } // Increase this element and return

    // Find the rightmost element to increase
    while( i != first )
    {
      *i = 0; // reset the right-hand element
      --i; // point to the left adjacent
      if(*i < max) { ++(*i); return true; } // Increase this element and return
    }

    return false;
  }
};

}
#endif
