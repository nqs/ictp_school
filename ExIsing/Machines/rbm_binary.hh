#include <iostream>
#include <Eigen/Dense>
#include <random>
#include <vector>

#ifndef NQS_RBMBINARY_HH
#define NQS_RBMBINARY_HH

namespace nqs{

using namespace std;
using namespace Eigen;

//Binary Valued Rbm
class RbmBinary{

  //number of visible units
  const int nv_;

  //number of hidden units
  const int nh_;

  //number of parameters
  int npar_;

  //weights
  MatrixXd W_;

  //visible units bias
  VectorXd a_;

  //hidden units bias
  VectorXd b_;

  VectorXd thetas_;
  VectorXd lnthetas_;
  VectorXd thetasnew_;
  VectorXd lnthetasnew_;

public:

  RbmBinary(int nv,int nh):
            nv_(nv),nh_(nh),W_(nv,nh),a_(nv),b_(nh),thetas_(nh),
            lnthetas_(nh),thetasnew_(nh),lnthetasnew_(nh)
  {

    npar_=nv_+nh_+nv_*nh_;

    cout<<"# RBM Initizialized with nvisible = "<<nv_<<" and nhidden = "<<nh_<<endl;

  }

  typedef double StateType;

  int Nvisible()const{
    return nv_;
  }

  int Nhidden()const{
    return nh_;
  }

  int Npar()const{
    return npar_;
  }

  void ProbHiddenGivenVisible(const VectorXd & v,VectorXd & probs,double beta=1){
    logistic(beta*(W_.transpose()*v+b_),probs);
  }

  void ProbVisibleGivenHidden(const VectorXd & h,VectorXd & probs,double beta=1){
    logistic(beta*(W_*h+a_),probs);
  }

  double Energy(const VectorXd & v,const VectorXd & h){
    return -(h.dot(W_.transpose()*v)+v.dot(a_)+h.dot(b_));
  }

  void InitRandomPars(int seed,double sigma){
    std::default_random_engine generator(seed);
    std::normal_distribution<double> distribution(0,sigma);

    for(int i=0;i<nv_;i++){
      a_(i)=distribution(generator);
    }
    for(int j=0;j<nh_;j++){
      b_(j)=distribution(generator);
    }

    for(int i=0;i<nv_;i++){
      for(int j=0;j<nh_;j++){
        W_(i,j)=distribution(generator);
      }
    }
  }

  VectorXd DerLog(const VectorXd & v){
    VectorXd der(npar_);

    for(int k=0;k<nv_;k++){
      der(k)=v(k);
    }

    logistic(W_.transpose()*v+b_,lnthetas_);

    for(int k=nv_;k<(nv_+nh_);k++){
      der(k)=lnthetas_(k-nv_);
    }

    int k=nv_+nh_;
    for(int i=0;i<nv_;i++){
      for(int j=0;j<nh_;j++){
        der(k)=lnthetas_(j)*v(i);
        k++;
      }
    }
    return der*0.5;
  }

  VectorXd GetParameters(){

    VectorXd pars(npar_);

    for(int k=0;k<nv_;k++){
      pars(k)=a_(k);
    }
    for(int k=nv_;k<(nv_+nh_);k++){
      pars(k)=b_(k-nv_);
    }

    int k=nv_+nh_;
    for(int i=0;i<nv_;i++){
      for(int j=0;j<nh_;j++){
        pars(k)=W_(i,j);
        k++;
      }
    }

    return pars;
  }

  void SetParameters(const VectorXd & pars){
    for(int k=0;k<nv_;k++){
      a_(k)=pars(k);
    }
    for(int k=(nv_);k<(nv_+nh_);k++){
      b_(k-nv_)=pars(k);
    }
    int k=(nv_+nh_);

    for(int i=0;i<nv_;i++){
      for(int j=0;j<nh_;j++){
        W_(i,j)=pars(k);
        k++;
      }
    }
  }

  //Value of the logarithm of the wave function
  double LogVal(const VectorXd & v){
    ln1pexp(W_.transpose()*v+b_,lnthetas_);

    return (v.dot(a_)+lnthetas_.sum())*0.5;
  }

  //Difference between logarithms of values, when one or more visible variables are being flipped
  //Newconf contains the new configuration, but here it is not used
  VectorXd LogValDiff(const VectorXd & v,const vector<vector<int> >  & toflip,const vector<vector<double>> & newconf){

    VectorXd logvaldiffs;

    const int nconn=toflip.size();
    logvaldiffs.resize(nconn);

    thetas_=(W_.transpose()*v+b_);
    ln1pexp(thetas_,lnthetas_);

    double logtsum=lnthetas_.sum();

    for(int k=0;k<nconn;k++){
      logvaldiffs(k)=0;

      if(toflip[k].size()!=0){

        thetasnew_=thetas_;

        for(int s=0;s<toflip[k].size();s++){
          const int sf=toflip[k][s];

          logvaldiffs(k)+=a_(sf)*(1-2*v(sf));

          thetasnew_+=(1-2*v(sf))*W_.row(sf);
        }

        ln1pexp(thetasnew_,lnthetasnew_);
        logvaldiffs(k)+=lnthetasnew_.sum() - logtsum;

      }
    }
    return logvaldiffs*0.5;
  }

  //Difference between derivatives of logarithms of values, when one or more visible variables are being flipped
  //Newconf contains the new configuration, but here it is not used since it is redundant information
  MatrixXd DerLogDiff(const VectorXd & v,const vector<vector<int> >  & toflip,const vector<vector<double>> & newconf){

    MatrixXd logvaldiffs;

    const int nconn=toflip.size();
    logvaldiffs=MatrixXd::Zero(nconn,npar_);

    thetas_=(W_.transpose()*v+b_);
    logistic(thetas_,lnthetas_);

    for(int k=0;k<nconn;k++){

      if(toflip[k].size()!=0){
        thetasnew_=thetas_;

        auto vprime=v;

        for(int s=0;s<toflip[k].size();s++){

          const int sf=toflip[k][s];
          thetasnew_+=(1-2*v(sf))*W_.row(sf);

          //visible bias part
          logvaldiffs(k,sf)+=(1-2*v(sf));

          vprime(sf)=(1-v(sf));
        }

        logistic(thetasnew_,lnthetasnew_);

        int p=nv_;

        //hidden bias part
        for(int j=0;j<nh_;j++){
          logvaldiffs(k,p)=lnthetasnew_(j)-lnthetas_(j);
          p++;
        }

        for(int i=0;i<nv_;i++){
          for(int j=0;j<nh_;j++){
            logvaldiffs(k,p)=(lnthetasnew_(j)*vprime(i)-lnthetas_(j)*v(i));
            p++;
          }
        }
      }
    }
    return logvaldiffs*0.5;
  }

  void logistic(const VectorXd & x,VectorXd & y){
    for(int i=0;i<x.size();i++){
      y(i)=logistic(x(i));
    }
  }

  inline double logistic(double x)const{
    return 1./(1.+std::exp(-x));
  }

  void ln1pexp(const VectorXd & x,VectorXd & y){
    for(int i=0;i<x.size();i++){
      y(i)=ln1pexp(x(i));
    }
  }

  //log(1+e^x)
  inline double ln1pexp(double x)const{
    if(x>30){
      return x;
    }
    // return std::log(1.+std::exp(x));
    return std::log1p(std::exp(x));
  }

  double RatioVisibleFlip(const VectorXd & v,const VectorXd & h,const vector<int> & toflip){
    double ratio=0;
    for(int k=0;k<toflip.size();k++){
      const int sf=toflip[k];
      assert(sf<nv_ && sf>=0);

      ratio+=h.dot(W_.row(sf))*(1-2*v(sf));
      ratio+=(1-2.*v(sf))*a_(sf);
    }
    return std::exp(ratio);
  }

  double RatioHiddenFlip(const VectorXd & v,const VectorXd & h,const vector<int> & toflip){
    double ratio=0;
    for(int k=0;k<toflip.size();k++){
      const int sf=toflip[k];
      assert(sf<nh_ && sf>=0);

      ratio+=v.dot(W_.col(sf))*(1-2*h(sf));
      ratio+=(1-2.*h(sf))*b_(sf);
    }
    return std::exp(ratio);
  }

  template<class Rng> void RandomVals(VectorXd & hv,Rng & rgen){
    std::uniform_int_distribution<int> distribution(0,1);

    for(int i=0;i<hv.size();i++){
      hv(i)=distribution(rgen);
    }
  }
  template<class Rng> void RandomValsWithProb(VectorXd & hv,const VectorXd & probs,Rng & rgen){
    std::uniform_real_distribution<double> distribution(0,1);

    for(int i=0;i<hv.size();i++){
      hv(i)=distribution(rgen)<probs(i);
    }
  }

};


}

#endif
