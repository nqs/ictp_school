**SEE LICENSE.md for the copyright statement**  

**Codes requirements**  

1. a modern c++ MPI compiler (compatible with c++11 standards)  

2. the eigen library (https://eigen.tuxfamily.org).  
   This library is header-only, thus you can just download and copy it into a  
   folder where the compiler can find it.  

The user then should change the Makefile according to its system configuration.  
Most notably, it should change the "EIGEN_DIR" variable with the directory  
including the eigen distribution.  


**Using the code**

1. Go to Examples/  
2. have a look at ising1d.cc, changing if needed the parameters used  
3. compile the code with 'Make'  
4. plot the results using the python script "python plot_ising.py"   
5. at the end of the training, you should get an error of order 1.0e-4  
   as shown in the plot 'ising1d.pdf'  
